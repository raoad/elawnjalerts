﻿Option Strict Off
Option Explicit On

Imports System
Imports eLaw.Utility
Imports eLaw.DBaccess
Imports eWatch.BusinessProcess
Imports eLaw.BusinessProcess
Imports eLawNJ.BusinessProcess
Public Class AlertsNJ

    '******************************************************************************
    ' Description: AlertsNJ class
    '
    ' Notes:       Each object contains one module-level database access object.
    '              The database connection is established when needed, and it is
    '              closed/torn-down immediately after use.
    '
    ' Date       Who  Comments
    ' ---------- ---- -------------------------------------------------------------
    '  08/20/2004 RR    Added GenerateInitialAlert
    '******************************************************************************
    Private m_objDB As New DBAccessUtils
    Private NULL_DATE As Date 'Un-initilized date variable

    Private Const colCaseID As String = "Case_Id"
    Private Const colCaseWatchID As String = "Casewatch_Id"
    Private Const colYearNumber As String = "year_nb"
    Private Const colIndexNumber As String = "index_nb"
    Private Const colCounty As String = "cas_1_cnty"
    Private Const ColPacerDatatx As String = "PacerData_tx"
    Private Const ColAsOfData As String = "AsOfData_dt"
    Private Const colFeeTypeCode As String = "FeeType_cd"
    Private Const ColProcessed As String = "processed_dt"


    Private m_objChangeBatches As eLawNJ.BusinessProcess.ChangeBatches
    Private m_objCaseChanges As eLawNJ.BusinessProcess.CaseChanges

    'Private NULL_DATE As Date
    Private Const COMPONENT_NAME As String = "eLAWNJ"
    Private Const SSLURL As String = "SSLURL"
    Private Const APPEARANCE_ADD As String = "*** Appearance Scheduled***"
    Private Const APPEARANCE_CHANGE As String = "*** Appearance Held ***"
    Private Const APPEARANCE_CANCELED As String = "*** Appearance Canceled ***"
    Private Const APPEARANCE_RESCHEDULED As String = "*** Appearance Rescheduled ***"
    Private Const APPEARANCE_UPDATE As String = "*** Appearance Update ***"
    Private Const LAWCASE_CHANGE As String = "*** Case Changes ***"
    Private Const PARTY_CHANGE As String = "*** Party Changes ***"
    Private Const DOCUMENTS_ADD As String = "*** Documents ***"
    Private Const ATTORNEY_ADD As String = "*** Attorney of Record Added ***"

    ''******************************************************************************
    '' Syntax:      lngStatus = objAlertsNY.GenerateInitialAlert()
    ''
    '' Description: Generate the initial alert for a CaseWatch (E-Watch)
    ''
    '' Arguments:   lngStatus (OUTPUT) =   True: Successful, no error
    ''
    ''
    '' Notes:       (none)
    ''
    '' Date       Who  Comments
    '' ---------- ---- -------------------------------------------------------------
    '' 08/20/2004 RR   First Created
    ''
    '' © Copyright 2004  eLaw, LLC.
    ''******************************************************************************
    Public Function GenerateInitialAlert() As Boolean
        Dim lngStatus As Integer
        Dim blnStatus As Boolean
        Dim blnTotalStatus As Boolean
        Dim strCaseName As String
        Dim strPrefix As String
        Dim strSubject As String
        Dim strMessage As String
        Dim strCourtSystem As String
        Dim objSysSet As eLaw.BusinessProcess.SystemSetting
        Dim objEmail As eLawEmailQueue.Emails
        Dim objEWatch As CaseWatch
        Dim objCourtSys As New CourtSystem
        Dim lngCaseWatchID As Integer
        Dim lngCaseID As Integer
        Dim strFeeTypeCode As String
        Dim dtAsofDate As Date
        Dim dtLastAlert As Date
        Dim strMycaseName As String

        Dim dtResult As DataTable

        Try

            GenerateInitialAlert = True
            blnTotalStatus = True

            dtResult = EWatchDA.GetNJCaseWatchesForInitialAlert()

            If (dtResult Is Nothing Or dtResult.Rows.Count = 0) Then
                GenerateInitialAlert = True
                Exit Function
            End If

            For Each caseWatchRow As DataRow In dtResult.Rows
                lngCaseWatchID = caseWatchRow(colCaseWatchID)
                lngCaseID = caseWatchRow(colCaseID)
                strFeeTypeCode = caseWatchRow(colFeeTypeCode)

                If lngCaseWatchID = 0 Or lngCaseID = 0 Then
                    blnTotalStatus = False
                End If
                'Get Systemsetting for email
                objSysSet = New SystemSetting

                'E-mail Subject
                If objSysSet.GetByName(EMAIL_CHANGES_SUBJECT & "_" & COURT_SYS_NJ) Then
                    strSubject = objSysSet.SettingValue
                Else
                    strSubject = DEFAULT_EMAIL_SUBJECT
                End If
                'E-mail Prefix
                If objSysSet.GetByName(EMAIL_INITIAL_CHANGES_PREFIX & "_" & COURT_SYS_NJ) Then
                    strPrefix = objSysSet.SettingValue & vbCrLf & vbCrLf
                Else
                    strPrefix = ""
                End If
                objEWatch = New CaseWatch
                blnStatus = True
                If objEWatch.GetByID(lngCaseWatchID) = False Then
                    blnTotalStatus = False
                    blnStatus = False
                End If
                If blnStatus Then
                    strMycaseName = objEWatch.MyCaseName
                    If objCourtSys.GetByCode(COURT_SYS_NJ) Then
                        strCourtSystem = objCourtSys.Name
                    End If
                    dtLastAlert = objEWatch.LastAlert

                    'Construct CaseName for e-mail Prefix:
                    objEmail = New eLawEmailQueue.Emails
                    strMessage = ""
                    strCaseName = objEmail.GetNJCaseInfoForEMail(lngCaseID)
                    If strCaseName = "" Then
                        objEmail = Nothing
                        objEWatch = Nothing
                        objSysSet = Nothing
                        blnStatus = False
                        blnTotalStatus = False
                    Else
                        strPrefix = Replace(strPrefix, EMAIL_CASENAME, strCaseName, , , CompareMethod.Text)
                    End If
                    strPrefix = Replace(strPrefix, "##NAME##", strMycaseName, , , CompareMethod.Text)
                    strPrefix = Replace(strPrefix, "##MYCASENAME##", strMycaseName, , , CompareMethod.Text)
                    strPrefix = Replace(strPrefix, "##REQUESTDATE##", objEWatch.CreateDate.ToString("MMMM d, yyyy "), , , CompareMethod.Text)
                    strPrefix = Replace(strPrefix, "##CURRENTDATE##", Now.ToString("MMMM d, yyyy "), , , CompareMethod.Text)

                    If objEWatch.FeeTypeCode = APP_DOCKET_ALERT_CODE Then
                        strPrefix = Replace(strPrefix, "##SERVICENAME##", APP_DOCKET_SERVICE_NJ, , , CompareMethod.Text)
                    ElseIf objEWatch.FeeTypeCode = APP_ALERT_CODE Then
                        strPrefix = Replace(strPrefix, "##SERVICENAME##", APP_APPEARANCE_SERVICE_NJ, , , CompareMethod.Text)
                    End If

                    If objEmail.CreateEMailMessageNJ(lngCaseID, COURT_SYS_NJ, dtLastAlert, dtAsofDate, strMessage, strFeeTypeCode) = False Then
                        objEmail = Nothing
                        objEWatch = Nothing
                        objSysSet = Nothing
                        blnStatus = False
                        blnTotalStatus = False
                    End If

                    'Queue the e-mail when message construction is successful
                    If strMessage <> "" Then
                        If objEmail Is Nothing Then
                            GenerateInitialAlert = False : Exit Function
                        End If
                        strMessage = strPrefix & strMessage
                        '9/13/2000 YL: Pass TrxCodePrefix as empty string
                        '              to avoid logging transaction
                        '8/21/2004 RR: The above comment is invalid. If you don't log
                        '               log the transaction, the email won't be sent.
                        '               look at GetEmailSentByEmailSentDate stored proc
                        '               in the eLaw database
                        lngStatus = objEmail.QueueByCaseWatch(lngCaseWatchID, strSubject, strMessage, NULL_DATE, True, True, "ESI", , COURT_SYS_NJ)
                        objEmail = Nothing
                        If lngStatus <> 0 Then 'Success
                            blnTotalStatus = False
                            blnStatus = False
                        End If
                    End If
                End If

                objEWatch = Nothing
                objSysSet = Nothing
            Next
            'Final result is FALSE even when only 1 failure
            If blnTotalStatus Then
                GenerateInitialAlert = True
            End If
            Exit Function
        Catch ex As Exception
            GenerateInitialAlert = False
            Throw ex
            Throw
        End Try
    End Function

    '***********************************************************************
    ' ---------- ---- -------------------------------------------------------------
    ' 02/17/2003 AD   First Created
    '
    ' © Copyright 2002  eLaw, Inc.
    '******************************************************************************
    Public Function Generate(Optional ByVal intUserDomain As Short = 1) As Boolean
        Dim objEmail As New eLawEmailQueue.Emails
        Dim strEmailSubject As String
        Dim objSysSet As eLaw.BusinessProcess.SystemSetting
        Dim blnTotalStatus As Boolean
        Dim blnBatchStatus As Boolean
        Dim blnStatus As Boolean
        Dim strEmailPrefix As String
        Try


            objSysSet = New eLaw.BusinessProcess.SystemSetting
            '**** Get Subject from systemsettings

            If objSysSet.GetByName(EMAIL_CHANGES_SUBJECT_NJ) Then
                strEmailSubject = objSysSet.SettingValue
            Else
                strEmailSubject = DEFAULT_EMAIL_SUBJECT
            End If

            If objSysSet.GetByName(EMAIL_CHANGES_PREFIX_NJ) Then
                strEmailPrefix = objSysSet.SettingValue & vbCrLf
            Else
                strEmailPrefix = ""
            End If

            objSysSet = Nothing

            blnTotalStatus = True
            'Instantiate classes from the corresponding Court System component
            m_objChangeBatches = New eLawNJ.BusinessProcess.ChangeBatches
            m_objCaseChanges = New eLawNJ.BusinessProcess.CaseChanges

            If m_objChangeBatches.GetForEmailProcessing = False Then
                m_objChangeBatches = Nothing
                m_objCaseChanges = Nothing
                Generate = False : Exit Function
            End If

            'Enumerate all Changed Batches/Cases to generate alert(s)
            blnTotalStatus = True
            For Each changeBatchRow As DataRow In m_objChangeBatches.ChangeBatchesDetail.Rows
                blnBatchStatus = True
                Dim _changeBatch As New ChangeBatch
                'Each Changed Batch
                With _changeBatch
                    .GetByID(changeBatchRow("ChangeBatch_ID"))
                    m_objCaseChanges.GetUnprocessedByChangeBatch(.ID)
                    If ProcessCaseChanges(m_objDB, m_objCaseChanges) = False Then
                        blnBatchStatus = False
                        blnTotalStatus = False
                    End If
                    '            If ProcessAppearanceSchedules(m_objDB, m_objCaseChanges) = False Then
                    '                blnBatchStatus = False
                    '                blnTotalStatus = False
                    '            End If
                    If blnBatchStatus Then
                        blnStatus = .SetEmailsComplete(.ID)
                        blnTotalStatus = blnTotalStatus And blnStatus
                    End If
                End With
            Next
            m_objChangeBatches = Nothing
            m_objCaseChanges = Nothing

            'Final result is FALSE even when only 1 Batch failed
            Generate = blnTotalStatus
            Exit Function
            objEmail = Nothing
            Generate = blnTotalStatus
            Exit Function
        Catch ex As Exception
            Generate = False
            Throw ex
        End Try
    End Function

    Private Function ProcessCaseChanges(ByRef objDB As DBAccessUtils, ByRef objCaseChanges As CaseChanges) As Boolean
        Dim lngIndex As Integer
        Dim lngStartIndex As Integer
        Dim lngBatch As Integer
        Dim lngPrevCase As Integer
        Dim lngPrevMotion As Integer
        Dim lngCase As Integer
        Dim lngStatus As Integer
        Dim blnStatus As Boolean
        Dim blnCaseStatus As Boolean
        Dim blnTotalStatus As Boolean
        Dim blnChanged As Boolean
        Dim strEmailSubject As String
        Dim strEmailPrefix As String
        Dim strMessage As String
        Dim strPrefix As String
        Dim strSectionA As String
        Dim strAppearances As String
        Dim strCourtSystem As String
        Dim objSysSet As SystemSetting
        Dim objEmail As New eLawEmailQueue.Emails
        Dim objCaseWatches As New CaseWatches
        Dim objCase As CaseSummary
        Dim objAppearance As AppearanceSummary
        Dim objAttorneys As AttorneySummaries
        Dim objAttorney As AttorneySummary
        Dim objCourtSys As New CourtSystem
        Dim strAlertAttr As String
        Dim strCaseHeader As String
        Dim strAttorneys As String
        Dim strActions As String = String.Empty
        Dim strMotions As String
        Dim strSummons As String = String.Empty
        Dim strOrders As String = String.Empty
        Dim strMiscellaneous As String = String.Empty
        Dim objCaseChangeType As CaseChangeType
        Dim Attorney_A As String = String.Empty
        Dim Summons_A As String = String.Empty
        Dim Orders_A As String = String.Empty
        Dim InterimActions_A As String = String.Empty
        Dim Miscellaneous_A As String = String.Empty
        Dim objDocuments As CarrierSummaries = Nothing
        Dim strCommentChanges As String = String.Empty
        Dim objMotion As MotionSummary
        Dim objDictionary As Dictionary
        'Dim objParty As Object
        Dim strCase As String
        Dim strParty As String
        Dim strFieldName As String
        Dim LawCase_U_status As String = String.Empty
        Dim LawCase_U_TransferTo As String
        Dim LawCase_U_TransferFrom As String
        Dim Lawcase_U_Comment As String = String.Empty
        Dim Motion_U_Filed As String
        Dim Motion_U_Status As String = String.Empty
        Dim Motion_U_Return As String
        Dim Motion_U_Court As String
        Dim Motion_U_Comment As String
        Dim Motion_U_ProcType As String
        Dim Motion_U_ProcStatus As String
        Dim Motion_U_Judge As String
        Dim Motion_U_Type As String
        Dim PARTY_U_status As String
        Dim Party_U_Name As String = String.Empty
        Dim Party_U_Date As String
        '***********When an Appearance is Scheduled, Rescheduled or Cancelled
        Dim Appearance_A As String = String.Empty
        Dim Appearance_U_Judge As String
        Dim Appearance_U_Proc_Type As String = String.Empty
        Dim Appearance_U_Proc_Status As String
        Dim Appearance_U_Motion_Status As String
        Dim Appearance_U_Court_Room As String
        Dim Party_A As String
        Dim strCourtRoom As String
        Dim strOldValue As String
        Dim strNewValue As String
        'Dim ObjParties As Object
        Dim lngMotionID As Integer
        Dim lngMotionDocNumber As Integer
        Dim lngAppearanceID As Integer
        Dim strMotionSummary As String
        Dim strTime As String
        Dim lngPrevAppearance As Integer
        Dim strTransferChanges As String = String.Empty

        Dim rstAlertType As DataTable
        Dim rstMotions As DataTable
        Dim rstResult As DataTable

        Try

            'Verify there is at least one Case Change record
            If objCaseChanges.Count = 0 Then
                ProcessCaseChanges = True : Exit Function
            End If

            blnCaseStatus = True
            blnTotalStatus = True
            blnChanged = False
            'lngStartIndex = 1
            lngPrevCase = -999999999
            strPrefix = ""
            strAppearances = ""
            strCase = ""
            strMotions = ""
            strParty = ""
            strAttorneys = ""
            strMessage = ""
            strAlertAttr = ""

            objCaseChangeType = New CaseChangeType
            rstAlertType = objCaseChangeType.GetAllCaseWatchAlertType("Y")

            If rstAlertType.Rows.Count = 0 Then ' DEFAULT VALUES  SHOULD NEVER HAPPEN JUST INCASE
                Appearance_A = "1"
                Attorney_A = "2"
                Summons_A = "4"
                Orders_A = "3"
                InterimActions_A = "19"
                Miscellaneous_A = "20"
            End If

            For Each alertType As DataRow In rstAlertType.Rows
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "APPEARANCE" Then
                    Appearance_A = alertType("alert_id")
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "APPEARANCE" And UCase(alertType("field_nm")) = "PROCEEDING_TYPE" Then
                        Appearance_U_Proc_Type = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "APPEARANCE" And UCase(alertType("field_nm")) = "COURT_ROOM" Then
                        Appearance_U_Court_Room = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "APPEARANCE" And UCase(alertType("field_nm")) = "PROCEEDING_STATUS" Then
                        Appearance_U_Proc_Status = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "APPEARANCE" And UCase(alertType("field_nm")) = "MOTION_STATUS" Then
                        Appearance_U_Motion_Status = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "APPEARANCE" And UCase(alertType("field_nm")) = "JUDGE_ID" Then
                        Appearance_U_Judge = alertType("alert_id")
                    End If
                End If
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "ATTORNEY" Then
                    Attorney_A = alertType("alert_id")
                End If
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "PARTY" Then
                    Party_A = alertType("alert_id")
                End If
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "ORDERINQUIRY" Then
                    Orders_A = alertType("alert_id")
                End If
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "SUMMONS" Then
                    Summons_A = alertType("alert_id")
                End If
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "INTERIMACTIONS" Then
                    InterimActions_A = alertType("alert_id")
                End If
                If UCase(alertType("transactiontype_cd")) = "A" And UCase(alertType("table_nm")) = "MISCELLANEOUSDOCUMENTS" Then
                    Miscellaneous_A = alertType("alert_id")
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "LAWCASE" And UCase(alertType("field_nm")) = "CASE_STATUS" Then
                        LawCase_U_status = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "LAWCASE" And UCase(alertType("field_nm")) = "TRANSFER_TO_VENUE" Then
                        LawCase_U_TransferTo = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "LAWCASE" And UCase(alertType("field_nm")) = "TRANSFER_FROM_VENUE" Then
                        LawCase_U_TransferFrom = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "LAWCASE" And UCase(alertType("field_nm")) = "CASE_COMMENT" Then
                        Lawcase_U_Comment = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "DATE_FILED" Then
                        Motion_U_Filed = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "MOTION_STATUS" Then
                        Motion_U_Status = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "RETURN_DATE" Then
                        Motion_U_Return = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "COURT_ROOM_NUMBER" Then
                        Motion_U_Court = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "COMMENT1" Then
                        Motion_U_Comment = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "PROCEEDING_TIME" Then
                        Motion_U_ProcType = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "PROCEEDING_STATUS" Then
                        Motion_U_ProcStatus = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "JUDGE_ID_CODE" Then
                        Motion_U_Judge = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "MOTION" And UCase(alertType("field_nm")) = "MOTION_TYPE" Then
                        Motion_U_Type = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "PARTY" And UCase(alertType("field_nm")) = "PARTY_STATUS" Then
                        PARTY_U_status = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "PARTY" And UCase(alertType("field_nm")) = "PARTY_NAME" Then
                        Party_U_Name = alertType("alert_id")
                    End If
                End If
                If Not IsDBNull(alertType("field_nm")) Then
                    If UCase(alertType("transactiontype_cd")) = "U" And UCase(alertType("table_nm")) = "PARTY" And UCase(alertType("field_nm")) = "DISP_DATE2" Then
                        Party_U_Date = alertType("alert_id")
                    End If
                End If
            Next

            'Get Systemsetting for email
            objSysSet = New SystemSetting
            'Get Subject from systemsettings
            If objSysSet.GetByName(EMAIL_CHANGES_SUBJECT) Then
                strEmailSubject = objSysSet.SettingValue
            Else
                strEmailSubject = DEFAULT_EMAIL_SUBJECT
            End If
            'Get Prefix from systemsettings
            If objSysSet.GetByName(EMAIL_CHANGES_PREFIX_NJ) Then
                strEmailPrefix = objSysSet.SettingValue & vbCrLf & vbCrLf
            Else
                strEmailPrefix = ""
            End If

            'Get Court System name
            If objCourtSys.GetByCode(COURT_SYS_NJ) Then
                strCourtSystem = objCourtSys.Name
            Else
                strCourtSystem = "State of New Jersey, Superior Court"
            End If

            objCaseWatches = New CaseWatches

            While lngIndex < objCaseChanges.Count
                'Only one batch will be processed at one time
                Dim _caseChange As CaseChange = objCaseChanges.GetItemByIndex(lngIndex)
                lngBatch = _caseChange.BatchID()
                'Each Case Change item (each Case may have many)
                lngCase = _caseChange.CaseID

                If lngCase <> lngPrevCase Then
                    objCase = New CaseSummary
                    If lngIndex > 0 Then
                        If blnCaseStatus Then 'No error for the Case
                            If strAttorneys <> "" Or strSummons <> "" Or strOrders <> "" Or strActions <> "" Or strMiscellaneous <> "" Or strParty <> "" Or strCase <> "" Or strMotions <> "" Then
                                If strParty = "" Then
                                    If Not objCase Is Nothing Then
                                        If objCase.GetByID(lngPrevCase) Then
                                            If Not objCase.Detail Is Nothing Then
                                                objAttorneys = objCase.Detail.Attorneys
                                                If Not (objAttorneys Is Nothing) Then
                                                    strParty = objAttorneys.GetText(lngPrevCase)
                                                    strParty = "The attorneys of record for this case are: " & vbCrLf & vbCrLf & strParty
                                                    objAttorneys = Nothing
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                strMessage = strMessage & strCase & strSummons & strOrders & strActions & strMiscellaneous & strParty
                                If strSummons <> "" Or strOrders <> "" Or strActions <> "" Or strMiscellaneous <> "" Then
                                    strMessage = strMessage & vbCrLf & "To obtain a copy of any of the above listed documents, " & "logon to www.e-law.com" & vbCrLf
                                End If
                                strAttorneys = ""
                                strActions = ""
                                strSummons = ""
                                strOrders = ""
                                strMiscellaneous = ""
                                strCaseHeader = ""
                                strMotions = ""
                                strCase = ""
                                strParty = ""
                                strTransferChanges = ""
                            End If
                            If (strMessage <> "" Or strAppearances <> "") Then 'Alert text generated
                                'strMessage = strPrefix & strMessage
                                '
                                'RP: remove this later
                                lngStatus = objEmail.QueueByNJCase(lngPrevCase, strEmailSubject, strPrefix, strMessage, strAppearances, NULL_DATE, True, True, TRXCODE_ALERT_PREFIX, objDB, strAlertAttr, COURT_SYS_NJ)
                                If lngStatus = 0 Then 'Success
                                    blnStatus = True
                                    'Set processed date to CaseChange records
                                    blnStatus = objCaseChanges.GetItemByIndex(lngIndex).SetProcessed(lngPrevCase, lngBatch)
                                    If Not blnStatus Then
                                        blnTotalStatus = False
                                    End If
                                Else 'Failure
                                    blnTotalStatus = False
                                End If
                            Else
                                blnStatus = True 'Set processed date to CaseChange records
                                blnStatus = objCaseChanges.GetItemByIndex(lngIndex).SetProcessed(lngPrevCase, lngBatch)
                            End If
                        End If
                        blnCaseStatus = True 'Reset Case status
                        blnTotalStatus = True
                        lngStartIndex = lngIndex 'Set Case start index
                        strAppearances = ""
                    End If

                    blnChanged = False
                    lngPrevCase = lngCase
                    blnStatus = objCaseWatches.GetByCaseID(lngCase, COURT_SYS_NJ)
                    While (Not blnStatus Or objCaseWatches.count = 0) ' case is not on watch
                        lngIndex = lngIndex + 1 ' so lets point to the next change record
                        If lngIndex > (objCaseChanges.Count - 1) Then    ' no more record and the previous record  is not on watch
                            blnStatus = objCaseChanges.GetItemByIndex(lngStartIndex).SetProcessed(lngPrevCase, lngBatch)    ' so update the previous case
                            GoTo GetNext ' and goto end of outer loop
                        End If
                        lngCase = objCaseChanges.GetItemByIndex(lngIndex).CaseID ' get the case of the this record
                        If lngPrevCase <> lngCase Then ' if its a new case
                            blnStatus = objCaseChanges.GetItemByIndex(lngStartIndex).SetProcessed(lngPrevCase, lngBatch)    'update the case
                            lngPrevCase = lngCase ' set the previous to this case
                            blnStatus = objCaseWatches.GetByCaseID(lngCase, COURT_SYS_NJ) ' get the case watch count for the case
                        End If
                    End While

                    strAttorneys = ""
                    strActions = ""
                    strSummons = ""
                    strOrders = ""
                    strMotions = ""
                    strMiscellaneous = ""
                    strPrefix = strEmailPrefix 'Setup prefix from systemsettings
                    strMessage = ""
                    strAlertAttr = ""
                    strCase = ""
                    strParty = ""
                    strMotionSummary = ""
                    strTransferChanges = ""
                    strAppearances = ""

                    'Get the Case Information
                    objCase = New CaseSummary
                    If objCase.GetByID(lngCase) Then
                        strCaseHeader = objEmail.GetNJCaseInfoForEMail(lngCase)
                        'Replace CaseName in e-mail Prefix
                        strPrefix = Replace(strPrefix, EMAIL_CASENAME, strCaseHeader, , , CompareMethod.Text)
                        strMessage = ""
                        If strCaseHeader = "" Then
                            blnCaseStatus = False
                        End If
                    Else
                        blnCaseStatus = False
                    End If
                End If

                _caseChange = objCaseChanges.GetItemByIndex(lngIndex)
                '1. Appearance (new)
                '2. Appearance status / type change
                If (_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "APPEARANCE") Or (UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "APPEARANCE" And UCase(_caseChange.FieldName) = "PROCEEDING_STATUS") Or (UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "APPEARANCE" And UCase(_caseChange.FieldName) = "APR_DATETIME") Or (UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "APPEARANCE" And UCase(_caseChange.FieldName) = "COURT_ROOM") Or (UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "APPEARANCE" And UCase(_caseChange.FieldName) = "JUDGE_ID") Or (UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "APPEARANCE" And UCase(_caseChange.FieldName) = "PROCEEDING_TYPE") And blnCaseStatus Then
                    ' Or _
                    '(UCase$(objCaseChanges.Item(lngIndex).TransactionType) = "U" And _
                    'UCase$(objCaseChanges.Item(lngIndex).TableName) = "MOTION" And _
                    'UCase$(objCaseChanges.Item(lngIndex).FieldName) = "MOTION_STATUS") Or _
                    '(UCase$(objCaseChanges.Item(lngIndex).TransactionType) = "U" And _
                    'UCase$(objCaseChanges.Item(lngIndex).TableName) = "MOTION" And _
                    'UCase$(objCaseChanges.Item(lngIndex).FieldName) = "RETURN_DATE")
                    If strAlertAttr > "" Then
                        strAlertAttr = strAlertAttr & "," & Appearance_A
                    Else
                        strAlertAttr = Appearance_A
                    End If
                    If UCase(_caseChange.FieldName) = "PROCEEDING_STATUS" Then
                        If strAlertAttr > "" Then
                            strAlertAttr = strAlertAttr & "," & Appearance_U_Proc_Type
                        Else
                            strAlertAttr = Appearance_U_Proc_Type
                        End If
                    End If
                    If UCase(_caseChange.FieldName) = "MOTION_STATUS" Then
                        If strAlertAttr > "" Then
                            strAlertAttr = strAlertAttr & "," & Motion_U_Status
                        Else
                            strAlertAttr = Motion_U_Status
                        End If
                    End If
                    blnChanged = True

                    objAppearance = New AppearanceSummary
                    If objAppearance.GetByID(_caseChange.PrimaryKey) Then
                        strSectionA = ""
                        ' check for motion appearance
                        If objAppearance.MotionID <> 0 Then
                            objMotion = New MotionSummary
                            lngMotionID = objAppearance.MotionID
                            If lngMotionID <> lngPrevMotion Then
                                lngPrevMotion = lngMotionID
                                strMotionSummary = ""
                                lngMotionDocNumber = objAppearance.MotionDocumentNumber
                                If objMotion.GetByID(objAppearance.MotionID) Then
                                    objMotion.GetMotionSummaryText(strMotionSummary)
                                    strMotionSummary = "_______________________________________" & vbCrLf & vbCrLf & strMotionSummary & vbCrLf
                                    rstMotions = objMotion.GetMotionsDetailsLastNextApp(lngCase, lngMotionID, lngMotionDocNumber)
                                    For Each motion As DataRow In rstMotions.Rows
                                        If CStr(motion("Appear")) = "LAST" Then
                                            'check for motion appearance
                                            If UCase$(motion("Proceeding_Status")) = "CANCEL" Then
                                                strMotionSummary = strMotionSummary & vbCrLf & APPEARANCE_CANCELED & vbCrLf & vbCrLf
                                            ElseIf UCase$(motion("Proceeding_Status")) = "RSCHED" Then
                                                strMotionSummary = strMotionSummary & vbCrLf & APPEARANCE_RESCHEDULED & vbCrLf & vbCrLf
                                            End If
                                            strTime = Convert.ToDateTime(motion("appearance_dt")).ToString("h:mm tt")
                                            strMotionSummary = strMotionSummary & vbCrLf & "The most recent Appearance on this motion was scheduled on " & UCase(FormatDateTime(motion("appearance_dt"), Microsoft.VisualBasic.DateFormat.LongDate))
                                            If Trim(motion("Judge_name")) <> "" Then
                                                strMotionSummary = strMotionSummary & ", before Judge " & UCase(motion("Judge_name"))
                                            End If
                                            If Trim(motion("Court_Room")) <> "" Then
                                                strMotionSummary = strMotionSummary & ", in court room  " & motion("Court_Room")
                                            End If
                                            strMotionSummary = strMotionSummary & " at " & strTime & ", at which time the appearance was marked " & UCase(motion("Proceeding_display_Status")) & " by the court clerk." & vbCrLf & vbCrLf
                                        End If
                                    Next
                                    If strMotionSummary <> "" Then
                                        strAppearances = strAppearances & vbCrLf & strMotionSummary
                                    End If
                                End If
                            End If
                            If (_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "APPEARANCE" And UCase(Trim(objAppearance.Action)) <> "RSCHED") Or (_caseChange.TransactionType = "U" And UCase(_caseChange.TableName) = "APPEARANCE" And objAppearance.AppearanceDate > Today) Then
                                With objAppearance
                                    strSectionA = strSectionA & "On " & UCase(.AppearanceDate.ToString("MMMM d, yyyy")) & " at " & .AppearanceDate.ToString("h:mm tt")
                                    'FormatDateTime(.AppearanceDate, vbLongTime)
                                    strCourtRoom = Trim(.CourtRoom)
                                    strSectionA = strSectionA & " a "
                                    If Trim(.DisplayAppearanceType) <> "" Then
                                        strSectionA = strSectionA & UCase(Trim(.DisplayAppearanceType))
                                    End If
                                    If DateDiff(Microsoft.VisualBasic.DateInterval.Day, .AppearanceDate, Today) = 0 Then
                                        strSectionA = strSectionA & " was scheduled on this motion"
                                    ElseIf .AppearanceDate >= Today Then
                                        strSectionA = strSectionA & " is scheduled on this motion"
                                    Else
                                        strSectionA = strSectionA & " was scheduled on this motion"
                                    End If

                                    If Trim(.Judge) <> "" Then
                                        strSectionA = strSectionA & " before Judge " & UCase(Trim(.Judge))
                                    End If

                                    If strCourtRoom <> "" Then
                                        strSectionA = strSectionA & " in Court Room  " & strCourtRoom & "."
                                    End If

                                    If Trim(.DisplayAction) <> "" Then
                                        strSectionA = strSectionA & " The appearance is marked as " & UCase(Trim(.DisplayAction)) & "."
                                    End If

                                    If Trim(.Comment) <> "" Then
                                        strSectionA = strSectionA & vbCrLf & vbCrLf & " The following additional comments exist " & UCase(.Comment) & "."
                                    End If
                                End With
                            End If
                        Else
                            lngAppearanceID = objAppearance.ID
                            If lngAppearanceID <> lngPrevAppearance Then
                                lngPrevAppearance = lngAppearanceID
                                With objAppearance
                                    strSectionA = strSectionA & "On " & UCase(.AppearanceDate.ToString("MMMM d, yyyy")) & " at " & .AppearanceDate.ToString("h:mm tt")
                                    'FormatDateTime(.AppearanceDate, vbLongTime)
                                    strCourtRoom = Trim(.CourtRoom)

                                    strSectionA = strSectionA & " a "

                                    If Trim(.DisplayAppearanceType) <> "" Then
                                        strSectionA = strSectionA & UCase(Trim(.DisplayAppearanceType))
                                    End If

                                    If DateDiff(Microsoft.VisualBasic.DateInterval.Day, .AppearanceDate, Today) = 0 Then
                                        strSectionA = strSectionA & " was scheduled"
                                    ElseIf .AppearanceDate >= Today Then
                                        strSectionA = strSectionA & " is scheduled"
                                    Else
                                        strSectionA = strSectionA & " was scheduled"
                                    End If

                                    If Trim(.Judge) <> "" Then
                                        strSectionA = strSectionA & " before Judge " & UCase(Trim(.Judge))
                                    End If

                                    If strCourtRoom <> "" Then
                                        strSectionA = strSectionA & " in Court Room  " & strCourtRoom & "."
                                    End If

                                    If Trim(.DisplayAction) <> "" Then
                                        strSectionA = strSectionA & " The appearance is marked as " & UCase(Trim(.DisplayAction)) & "."
                                    End If

                                    If Trim(.Comment) <> "" Then
                                        strSectionA = strSectionA & vbCrLf & vbCrLf & "The following additional comments exist " & UCase(.Comment) & "."
                                    End If
                                End With
                            End If
                        End If
                        If strSectionA <> "" Then
                            If strAppearances <> "" Then
                                strAppearances = strAppearances & vbCrLf & vbCrLf
                            End If
                            If _caseChange.TransactionType = "A" Then
                                strAppearances = strAppearances & APPEARANCE_ADD & vbCrLf & vbCrLf
                            ElseIf _caseChange.TransactionType = "U" And UCase(_caseChange.TableName) = "APPEARANCE" Then
                                strAppearances = strAppearances & APPEARANCE_UPDATE & vbCrLf & vbCrLf
                            End If
                            strAppearances = strAppearances & strSectionA & vbCrLf & vbCrLf
                        End If

                    End If
                    objAppearance = Nothing
                End If

                '3. SUMMONS (new)

                If ((_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "SUMMONS")) And blnCaseStatus Then

                    If strAlertAttr > "" Then
                        strAlertAttr = strAlertAttr & "," & Summons_A
                    Else
                        strAlertAttr = Summons_A
                    End If

                    blnChanged = True

                    objDocuments = New CarrierSummaries
                    rstResult = objDocuments.GetDocumentById(_caseChange.PrimaryKey, "SUMMONS")
                    If (rstResult.Rows.Count > 0) Then
                        Dim result As DataRow = rstResult.Rows(0)
                        strSectionA = ""
                        With objDocuments
                            strSectionA = strSectionA & "A Summon " & result("doc_num") & ", was entered on " & UCase(Convert.ToDateTime(result("date_filed")).ToString("MMMM d, yyyy")) & " by " & UCase(Trim(CStr(result("Party")))) & ". "
                            If Trim(result("servedparty")) <> "" Then
                                strSectionA = strSectionA & " Served Party: " & UCase(Trim(result("servedparty")))
                            End If
                            If (Not IsDBNull(result("serveddate")) AndAlso Trim(result("serveddate")) <> "") Then
                                strSectionA = strSectionA & " Served Date: " & Trim(result("serveddate"))
                            End If
                            strSectionA = GlobalUtils.EndofSentence(strSectionA)
                        End With
                        strSectionA = strSectionA & vbCrLf
                        If strSectionA <> "" Then
                            If _caseChange.TransactionType = "A" Then
                                strSummons = strSummons & DOCUMENTS_ADD & vbCrLf & vbCrLf
                            End If
                            strSummons = strSummons & strSectionA & vbCrLf
                        End If
                    End If
                End If

                '4. ORDER INQUIRY (new)
                If ((_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "ORDERINQUIRY")) And blnCaseStatus Then

                    If strAlertAttr > "" Then
                        strAlertAttr = strAlertAttr & "," & Orders_A
                    Else
                        strAlertAttr = Orders_A
                    End If

                    blnChanged = True

                    objDocuments = New CarrierSummaries
                    rstResult = objDocuments.GetDocumentById(_caseChange.PrimaryKey, "ORDERINQUIRY")

                    If (rstResult.Rows.Count > 0) Then
                        Dim result As DataRow = rstResult.Rows(0)
                        strSectionA = ""
                        With objDocuments
                            strSectionA = strSectionA & "A " & result("DisplayType") & ", was entered on " & UCase(Convert.ToDateTime(result("date_filed")).ToString("MMMM d, yyyy")) & " by " & UCase(Trim(CStr(result("Party")))) & ". "
                            If Trim(result("Judge")) <> "" Then
                                strSectionA = strSectionA & " Judge: " & UCase(Trim(result("Judge")))
                            End If
                            If Trim(result("result")) <> "" Then
                                strSectionA = strSectionA & " Result: " & UCase(Trim(result("result")))
                            End If
                            strSectionA = GlobalUtils.EndofSentence(strSectionA)
                        End With
                        strSectionA = strSectionA & vbCrLf
                        If strSectionA <> "" Then
                            If _caseChange.TransactionType = "A" Then
                                strOrders = strOrders & DOCUMENTS_ADD & vbCrLf & vbCrLf
                            End If
                            strOrders = strOrders & strSectionA & vbCrLf
                        End If

                    End If
                End If

                '5. INTERIM ACTIONS (new)
                If ((_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "INTERIMACTIONS")) And blnCaseStatus Then

                    If strAlertAttr > "" Then
                        strAlertAttr = strAlertAttr & "," & InterimActions_A
                    Else
                        strAlertAttr = InterimActions_A
                    End If

                    blnChanged = True

                    objDocuments = New CarrierSummaries
                    rstResult = objDocuments.GetDocumentById(_caseChange.PrimaryKey, "INTERIMACTIONS")

                    If (rstResult.Rows.Count > 0) Then
                        Dim result As DataRow = rstResult.Rows(0)
                        strSectionA = ""
                        With objDocuments
                            strSectionA = strSectionA & "A " & result("DisplayType") & ", was entered on " & UCase(Convert.ToDateTime(result("date_filed")).ToString("MMMM d, yyyy")) & " by " & UCase(Trim(CStr(result("Party")))) & "."
                            If Trim(CStr(result("Comments1"))) <> "" Then
                                strSectionA = strSectionA & " Comments: " & UCase(Trim(CStr(result("Comments1"))))
                            End If
                            strSectionA = GlobalUtils.EndofSentence(strSectionA)
                        End With
                        strSectionA = strSectionA & vbCrLf
                        If strSectionA <> "" Then
                            If _caseChange.TransactionType = "A" Then
                                strActions = strActions & DOCUMENTS_ADD & vbCrLf & vbCrLf
                            End If
                            strActions = strActions & strSectionA & vbCrLf
                        End If

                    End If
                End If

                '6. MISCELLANEOUS DOCUMENT (new)
                If ((_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "MISCELLANEOUSDOCUMENTS")) And blnCaseStatus Then

                    If strAlertAttr > "" Then
                        strAlertAttr = strAlertAttr & "," & Miscellaneous_A
                    Else
                        strAlertAttr = Miscellaneous_A
                    End If

                    blnChanged = True

                    objDocuments = New CarrierSummaries
                    rstResult = objDocuments.GetDocumentById(_caseChange.PrimaryKey, "MISCELLANEOUSDOCUMENTS")

                    If (rstResult.Rows.Count > 0) Then
                        Dim result As DataRow = rstResult.Rows(0)
                        strSectionA = ""
                        With objDocuments
                            strSectionA = strSectionA & "A " & result("DisplayType") & ", was entered on " & UCase(Convert.ToDateTime(result("date_filed")).ToString("MMMM d, yyyy")) & " by " & UCase(Trim(CStr(result("Party")))) & "."
                            If Trim(CStr(result("Comments1"))) <> "" Then
                                strSectionA = strSectionA & " Comments: " & UCase(Trim(CStr(result("Comments1"))))
                            End If
                            strSectionA = GlobalUtils.EndofSentence(strSectionA)
                        End With
                        strSectionA = strSectionA & vbCrLf
                        If strSectionA <> "" Then
                            If _caseChange.TransactionType = "A" Then
                                strMiscellaneous = strMiscellaneous & DOCUMENTS_ADD & vbCrLf & vbCrLf
                            End If
                            strMiscellaneous = strMiscellaneous & strSectionA & vbCrLf
                        End If

                    End If
                End If
                '7. ATTORNEY(new)
                If ((_caseChange.TransactionType = "A" And UCase(_caseChange.TableName) = "ATTORNEY")) And blnCaseStatus Then

                    If strAlertAttr > "" Then
                        strAlertAttr = strAlertAttr & "," & Attorney_A
                    Else
                        strAlertAttr = Attorney_A
                    End If

                    blnChanged = True

                    objAttorney = New AttorneySummary

                    If objAttorney.GetByID(_caseChange.PrimaryKey) Then
                        strSectionA = ""
                        With objDocuments
                            strSectionA = strSectionA & "The following attorney is added to the case :" & vbCrLf & vbCrLf & vbTab & vbTab & objAttorney.Name & vbCrLf & vbTab & vbTab & objAttorney.Firm & vbCrLf & vbTab & vbTab & objAttorney.FirmAddress & vbCrLf & vbTab & vbTab & objAttorney.FirmCity & vbCrLf & vbTab & vbTab & objAttorney.FirmState & vbCrLf & vbTab & vbTab & objAttorney.FirmZip & vbCrLf & vbTab & vbTab & objAttorney.Phone & vbCrLf
                            strSectionA = GlobalUtils.EndofSentence(strSectionA)
                        End With

                        If strSectionA <> "" Then
                            If _caseChange.TransactionType = "A" Then
                                strAttorneys = strAttorneys & ATTORNEY_ADD & vbCrLf & vbCrLf
                            End If
                            strAttorneys = strAttorneys & strSectionA & vbCrLf
                        End If

                    End If
                End If

                ' 8 Update status in LawCase Table
                If Not IsDBNull(_caseChange.FieldName) Then
                    If UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "LAWCASE" And UCase(_caseChange.FieldName) = "CASE_STATUS" And blnCaseStatus Then

                        If strAlertAttr > "" Then
                            strAlertAttr = strAlertAttr & "," & LawCase_U_status
                        Else
                            strAlertAttr = LawCase_U_status
                        End If
                        blnChanged = False
                        strOldValue = Trim(_caseChange.OldValue) & ""
                        strNewValue = Trim(_caseChange.NewValue) & ""
                        If strNewValue <> "" Or strOldValue <> "" Then
                            blnChanged = True
                            strFieldName = _caseChange.LogicalFieldName
                            objDictionary = New Dictionary
                            If Not objDictionary Is Nothing Then
                                If objDictionary.GetByID(CInt(strNewValue)) Then
                                    strCommentChanges = " The Court Clerk reports that the Case" & " is now " & UCase(objDictionary.Value) & "." & vbCrLf
                                End If
                            End If
                            objDictionary = Nothing
                        End If

                        If blnChanged Then
                            strCase = strCase & LAWCASE_CHANGE & vbCrLf & vbCrLf
                            strCase = strCase & strCommentChanges & vbCrLf
                        End If
                    End If
                End If

                ' 9 Update comment in LawCase Table
                If Not IsDBNull(_caseChange.FieldName) Then
                    If UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "LAWCASE" And UCase(_caseChange.FieldName) = "CASE_COMMENT" And blnCaseStatus Then
                        If strAlertAttr > "" Then
                            strAlertAttr = strAlertAttr & "," & Lawcase_U_Comment
                        Else
                            strAlertAttr = Lawcase_U_Comment
                        End If
                        blnChanged = False
                        If _caseChange.NewValue <> "" Or _caseChange.OldValue <> "" Then
                            strFieldName = _caseChange.LogicalFieldName
                            strNewValue = Trim(_caseChange.NewValue)
                            strNewValue = Replace(strNewValue, "00 00 0000", "")

                            If Trim(strNewValue) <> "" Then
                                blnChanged = True
                                strCommentChanges = " The Clerk's " & strFieldName & " on the Docket have been amended as follows: " & UCase(strNewValue) & "." & vbCrLf
                            End If

                        End If
                        If blnChanged Then
                            strCase = strCase & LAWCASE_CHANGE & vbCrLf & vbCrLf
                            strCase = strCase & strCommentChanges & vbCrLf
                        End If
                    End If
                End If


                ' 10 Update transfer from or two in LawCase Table
                If Not IsDBNull(_caseChange.FieldName) Then
                    If UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "LAWCASE" And (UCase(_caseChange.FieldName) = "TRANSFER_TO_VENUE" Or UCase(_caseChange.FieldName) = "TRANSFER_FROM_VENUE") And blnCaseStatus Then

                        If strAlertAttr > "" Then
                            strAlertAttr = strAlertAttr & "," & Lawcase_U_Comment
                        Else
                            strAlertAttr = Lawcase_U_Comment
                        End If

                        blnChanged = False
                        If strTransferChanges = "" Then
                            If _caseChange.NewValue <> "" Or _caseChange.OldValue <> "" Then
                                blnChanged = True
                                objCase = New CaseSummary
                                If objCase.GetByID(_caseChange.CaseID) Then
                                    strFieldName = _caseChange.LogicalFieldName
                                    strTransferChanges = "The Case Venue has been transfered from " & objCase.TransferedFrom
                                    If objCase.TransferedTo <> "" Then
                                        strTransferChanges = strTransferChanges & " To " & objCase.TransferedTo
                                    End If
                                    objCase = Nothing
                                End If
                            End If

                        End If
                        If blnChanged Then
                            strCase = strCase & LAWCASE_CHANGE & vbCrLf & vbCrLf
                            strCase = strCase & vbCrLf & strTransferChanges
                        End If
                    End If
                End If


                ' 11 Update status in Party Table
                If Not IsDBNull(_caseChange.FieldName) Then
                    If UCase(_caseChange.TransactionType) = "U" And UCase(_caseChange.TableName) = "PARTY" And (UCase(_caseChange.FieldName) = "PARTY_NAME" Or UCase(_caseChange.FieldName) = "DISP_DATE2" Or UCase(_caseChange.FieldName) = "PTY_STATUS") And blnCaseStatus Then

                        If strAlertAttr > "" Then
                            strAlertAttr = strAlertAttr & "," & Party_U_Name
                        Else
                            strAlertAttr = Party_U_Name
                        End If


                        blnChanged = False
                        If strParty = "" Then
                            If _caseChange.NewValue <> "" And _caseChange.OldValue <> "" Then
                                blnChanged = True
                                objCase = New CaseSummary
                                If Not objCase Is Nothing Then
                                    If objCase.GetByID(_caseChange.CaseID) Then
                                        If Not objCase.Detail Is Nothing Then
                                            objAttorneys = objCase.Detail.Attorneys
                                            If Not (objAttorneys Is Nothing) Then
                                                strParty = objAttorneys.GetText(_caseChange.CaseID)
                                                objAttorneys = Nothing
                                            End If
                                        End If
                                    End If
                                    objCase = Nothing
                                End If
                            End If
                        End If
                        If blnChanged Then
                            strParty = "The attorneys and parties of record for this case are: " & vbCrLf & vbCrLf & GlobalUtils.EndofSentence(strParty)
                        End If
                    End If
                End If
GetNext:
                lngIndex = lngIndex + 1
            End While
            'Process the LAST or the ONLY Case
            If blnCaseStatus Then 'No error for the Case
                If strAttorneys <> "" Or strSummons <> "" Or strOrders <> "" Or strActions <> "" Or strMiscellaneous <> "" Or strParty <> "" Or strCase <> "" Or strMotions <> "" Then
                    If strParty = "" Then
                        objCase = New CaseSummary
                        If Not objCase Is Nothing Then
                            If objCase.GetByID(lngPrevCase) Then
                                If Not objCase.Detail Is Nothing Then
                                    objAttorneys = objCase.Detail.Attorneys
                                    If Not (objAttorneys Is Nothing) Then
                                        strParty = objAttorneys.GetText(lngPrevCase)
                                        strParty = "The attorneys of record for this case are: " & vbCrLf & vbCrLf & strParty
                                        objAttorneys = Nothing
                                    End If
                                End If
                            End If
                        End If
                        objCase = Nothing
                    End If

                    strMessage = strMessage & strCase & strMotions & strAttorneys & strSummons & strOrders & strActions & strMiscellaneous & strParty
                    strAttorneys = ""
                    strActions = ""
                    strSummons = ""
                    strOrders = ""
                    strMiscellaneous = ""
                    strCaseHeader = ""
                    strMotions = ""
                    strCase = ""
                    strParty = ""
                    strMotionSummary = ""
                    strTransferChanges = ""
                End If

                If (strMessage <> "" Or strAppearances <> "") Then
                    'strMessage = strPrefix & strMessage
                    'RP: remove this later
                    lngStatus = objEmail.QueueByNJCase(lngPrevCase, strEmailSubject, strPrefix, strMessage, strAppearances, NULL_DATE, True, True, TRXCODE_ALERT_PREFIX, objDB, strAlertAttr, COURT_SYS_NJ)
                    If lngStatus = 0 Then 'Success
                        blnStatus = True 'Set processed date to CaseChange records
                        blnStatus = objCaseChanges.GetItemByIndex(lngStartIndex).SetProcessed(lngPrevCase, lngBatch)
                        If Not blnStatus Then
                            blnTotalStatus = False
                        End If
                    Else 'Failure
                        blnTotalStatus = False
                    End If
                Else
                    blnStatus = True 'Set processed date to CaseChange records
                    blnStatus = objCaseChanges.GetItemByIndex(lngStartIndex).SetProcessed(lngPrevCase, lngBatch)
                End If
                strAppearances = ""
                strMessage = ""
            End If

            objCaseWatches = Nothing
            ProcessCaseChanges = blnTotalStatus
            Exit Function
        Catch ex As Exception
            ProcessCaseChanges = False
            Throw ex
        End Try
    End Function
End Class
